import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalculatorWrapperComponent } from './calculator-wrapper/calculator-wrapper.component';
import { CalcButtonComponent } from './calculator-wrapper/calc-button/calc-button.component';

@NgModule({
  declarations: [
    AppComponent,
    CalculatorWrapperComponent,
    CalcButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
